
local function endsWith(strIn, strMatch)
	if strIn == strMatch then return true end
	if #strIn < #strMatch then return false end
	return strIn:sub(#strIn - #strMatch) == strMatch
end

local function startsWith(strIn, strMatch)
	if strIn == strMatch then return true end
	if #strIn < #strMatch then return false end
	return strIn:sub(1, #strMatch) == strMatch
end

if mods['enemyracemanager'] then
	local factor = settings.startup['pollution-love-erm-scale-factor'].value
	local power = settings.startup['pollution-love-erm-scale-power'].value

	if factor ~= 0 then
		local races = {'erm_vanilla'}

		if mods['erm_toss'] then table.insert(races, 'erm_toss') end
		if mods['erm_zerg'] then table.insert(races, 'erm_zerg') end

		for name, data in pairs(data.raw.unit) do
			local hit = false

			for _, mod in ipairs(races) do
				if startsWith(name, mod) then
					hit = true
					break
				end
			end

			if hit then
				local determineLevel = select(3, name:find('([0-9]+)$'))

				if determineLevel and data.pollution_to_join_attack then
					determineLevel = tonumber(determineLevel) or 1
					data.pollution_to_join_attack = data.pollution_to_join_attack * (1 + math.pow((determineLevel - 1) * factor, power))
				end
			end
		end
	end
end

if settings.startup['pollution-love-pollution-pull'].value then
	data:extend({{
		type = 'electric-energy-interface',
		name = 'pollution-love-suction-interface',
		localised_name = {'entity-name.pollution-love-suction-interface'},
		localised_description = {'entity-description.pollution-love-suction-interface'},

		flags = {
			'not-on-map',
			'not-deconstructable',
			'not-blueprintable',
			'hidden',
			'not-flammable',
			'not-upgradable',
			'not-in-kill-statistics',
			'not-repairable',
			'placeable-off-grid',
		},

		icon = '__base__/graphics/icons/fluid/steam.png',
		icon_size = 64,
		icon_mipmaps = 4,

		max_health = 666,

		energy_production = '0W',
		energy_usage = '0W',

		selection_box = {
			{-0.5, -0.5},
			{0.5, 0.5}
		},

		collision_mask = {}, -- do not collide with anything

		energy_source = {
			type = 'electric',
			buffer_capacity = '10MJ',
			usage_priority = 'secondary-input',
			input_flow_limit = '10MW',
			output_flow_limit = '0W',
			drain = '0W',
		}
	}})
end
