
if not settings.startup['pollution-love-pollution-pull'].value then return end

local suction_values = {}
local filter = {}

if settings.startup['pollution-love-krastorio-2-air-filter'].value and script.active_mods['Krastorio2'] then
	table.insert(filter, {filter = 'name', name = 'kr-air-purifier'})

	suction_values['kr-air-purifier'] = {
		min_pollution_in_chunk = 75,
		pull_from = {
			{0, 32},
			{0, -32},
			{32, 0},
			{-32, 0},
		}
	}

	if script.active_mods['omnimatter_compression'] then
		local names = {
			'compact',
			'nanite',
			'quantum',
			'singularity',
		}

		local function diamond(size)
			local result = {}

			for x = -size, size do
				for y = -size, size do
					if x ~= 0 or y ~= 0 then
						local distance = math.abs(x) + math.abs(y)

						if distance <= size then
							table.insert(result, {x * 32, y * 32})
						end
					end
				end
			end

			return result
		end

		suction_values['kr-air-purifier-compressed-compact'] = {
			min_pollution_in_chunk = 75 * 4,
			pull_from = diamond(2)
		}

		suction_values['kr-air-purifier-compressed-nanite'] = {
			min_pollution_in_chunk = 75 * 16,
			pull_from = diamond(4)
		}

		suction_values['kr-air-purifier-compressed-quantum'] = {
			min_pollution_in_chunk = 75 * 64,
			pull_from = diamond(6)
		}

		suction_values['kr-air-purifier-compressed-singularity'] = {
			min_pollution_in_chunk = 75 * 256,
			pull_from = diamond(8)
		}

		table.insert(filter, {filter = 'name', name = 'kr-air-purifier-compressed-compact'})
		table.insert(filter, {filter = 'name', name = 'kr-air-purifier-compressed-nanite'})
		table.insert(filter, {filter = 'name', name = 'kr-air-purifier-compressed-quantum'})
		table.insert(filter, {filter = 'name', name = 'kr-air-purifier-compressed-singularity'})
	end
end

if #filter == 0 then return end

local air_filters, built

local function scan()
	local protos = {}

	for protoname in pairs(suction_values) do
		table.insert(protos, protoname)
	end

	for name, surface in pairs(game.surfaces) do
		local ents = surface.find_entities_filtered({name = protos})

		for _, ent in ipairs(ents) do
			built(ent)
		end
	end
end

script.on_init(function()
	global.air_filters = {}
	air_filters = global.air_filters
	scan()
end)

script.on_load(function()
	air_filters = global.air_filters
end)

script.on_configuration_changed(function(event)
	if event.mod_startup_settings_changed or event.migration_applied or next(event.mod_changes) then
		scan()
	end
end)

local position2 = {0, 0}

local math_min = math.min
local ipairs = ipairs
local pairs = pairs

script.on_event(defines.events.on_tick, function(event)
	if event.tick % settings.global['pollution-love-pollution-tickrate'].value ~= 0 then return end

	local joules_per_pollution = settings.global['pollution-love-pollution-cost'].value

	local entity

	global.last_index, entity = next(air_filters, global.last_index)

	if not global.last_index or not entity then return end

	if not entity.entity.valid or not entity.interface.valid then
		if entity.interface.valid then
			entity.interface.destroy()
		end

		air_filters[global.last_index] = nil
		return
	end

	local surface = entity.entity.surface
	local position = entity.entity.position

	local values = suction_values[entity.entity.name]

	if not values then
		air_filters[global.last_index] = nil
		return
	end

	local get_pollution = surface.get_pollution
	local pollution_in_this = get_pollution(position)
	local multiplier = settings.global['pollution-love-pollution-pull-multiplier'].value

	if pollution_in_this < values.min_pollution_in_chunk * multiplier then
		local energy = entity.interface.energy

		if energy > 0 then
			local pollute = surface.pollute

			local efficiency = settings.global['pollution-love-pollution-pull-efficiency'].value
			local inv = 1 / efficiency

			for _, offset in ipairs(values.pull_from) do
				position2[1] = position.x + offset[1]
				position2[2] = position.y + offset[2]

				local pollution = math_min(get_pollution(position2), values.min_pollution_in_chunk * multiplier - pollution_in_this, energy * joules_per_pollution * inv) * efficiency

				if pollution > 0 then
					pollution_in_this = pollution_in_this + pollution
					energy = energy - (pollution * joules_per_pollution) * inv
					pollute(position2, -pollution)
					pollute(position, pollution)

					if pollution_in_this >= values.min_pollution_in_chunk * multiplier or energy <= 0 then
						break
					end
				end
			end

			entity.interface.energy = energy
		end
	end
end)

function built(entity)
	local id = script.register_on_entity_destroyed(entity)
	if air_filters[id] then return end

	air_filters[id] = {
		entity = entity,
		interface = entity.surface.create_entity({
			name = 'pollution-love-suction-interface',
			position = entity.position,
			force = entity.force,
		})
	}

	air_filters[id].interface.destructible = false
	air_filters[id].interface.minable = false
	air_filters[id].interface.rotatable = false
end

script.on_event(defines.events.on_built_entity, function(event)
	built(event.created_entity)
end, filter)

script.on_event(defines.events.on_robot_built_entity, function(event)
	built(event.created_entity)
end, filter)

script.on_event(defines.events.script_raised_revive, function(event)
	built(event.entity)
end, filter)

script.on_event(defines.events.script_raised_built, function(event)
	built(event.entity)
end, filter)

script.on_event(defines.events.on_entity_cloned, function(event)
	built(event.destination)
end, filter)

script.on_event(defines.events.on_entity_destroyed, function(event)
	local data = air_filters[event.registration_number]

	if data then
		if data.interface.valid then
			data.interface.destroy()
		end

		air_filters[event.registration_number] = nil
		global.last_index = nil
	end
end)
