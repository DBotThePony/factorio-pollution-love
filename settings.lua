
data:extend({
	{
		type = 'string-setting',
		name = 'pollution-love-omnicrompression-pollution',
		setting_type = 'startup',
		order = 'pollution-love-a1',

		default_value = 'BOTH_BUT_CONSUMERS_LINEAR',

		allowed_values = {
			'NONE',
			'BOTH',
			'PRODUCERS',
			'CONSUMERS',
			'BOTH_BUT_CONSUMERS_LINEAR',
		}
	},

	{
		type = 'string-setting',
		name = 'pollution-love-omnicrompression-pollution-power',
		setting_type = 'startup',
		order = 'pollution-love-a2',

		default_value = 0.95,
	},

	{
		type = 'double-setting',
		name = 'pollution-love-omnicrompression-pollution-add',
		localised_description = {'mod-setting-description.pollution-love-omnicrompression-pollution-power'},
		setting_type = 'startup',
		order = 'pollution-love-a3',
		default_value = 0.5,
	},

	{
		type = 'double-setting',
		name = 'pollution-love-omnicrompression-pollution-scale',
		localised_description = {'mod-setting-description.pollution-love-omnicrompression-pollution-power'},
		setting_type = 'startup',
		order = 'pollution-love-a4',
		default_value = 1,
	},

	{
		type = 'double-setting',
		name = 'pollution-love-erm-scale-power',
		setting_type = 'startup',
		order = 'pollution-love-b1',
		default_value = 1,
	},

	{
		type = 'double-setting',
		name = 'pollution-love-erm-scale-factor',
		localised_description = {'mod-setting-description.pollution-love-erm-scale-power'},
		setting_type = 'startup',
		order = 'pollution-love-b2',

		default_value = 0.25,
		minimum_value = 0.01,
	},

	{
		type = 'bool-setting',
		name = 'pollution-love-pollution-pull',
		setting_type = 'startup',
		order = 'pollution-love-c-a1',

		default_value = true,
	},

	{
		type = 'bool-setting',
		name = 'pollution-love-krastorio-2-air-filter',
		setting_type = 'startup',
		order = 'pollution-love-c-a2',

		default_value = true,
	},

	{
		type = 'double-setting',
		name = 'pollution-love-pollution-cost',
		setting_type = 'runtime-global',
		order = 'pollution-love-c-b1',

		default_value = 400,
	},

	{
		type = 'double-setting',
		name = 'pollution-love-pollution-pull-efficiency',
		setting_type = 'runtime-global',
		order = 'pollution-love-c-b2',

		default_value = 0.5,
		minimum_value = 0.01,
		maximum_value = 1,
	},

	{
		type = 'double-setting',
		name = 'pollution-love-pollution-pull-multiplier',
		setting_type = 'runtime-global',
		order = 'pollution-love-c-b3',

		default_value = 1,
		minimum_value = 0.01,
		maximum_value = 1000,
	},

	{
		type = 'int-setting',
		name = 'pollution-love-pollution-tickrate',
		setting_type = 'runtime-global',
		order = 'pollution-love-c-b3',

		default_value = 10,
		minimum_value = 1,
	},
})
