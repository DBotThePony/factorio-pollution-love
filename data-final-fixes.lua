
if mods['omnimatter_compression'] then
	local setting = settings.startup['pollution-love-omnicrompression-pollution'].value

	if setting ~= 'NONE' then
		local types = {
			'assembling-machine', 'furnace', 'rocket-silo',
			'boiler', 'generator', 'mining-drill', 'beacon', 'offshore-pump', 'inserter',
			'burner-generator', 'roboport'
		}

		local mapped = {
			["compact"] = 1,
			["nanite"] = 2,
			["quantum"] = 3,
			["singularity"] = 4
		}

		local multiplier = settings.startup["omnicompression_multiplier"].value
		local power = settings.startup['pollution-love-omnicrompression-pollution-power'].value
		local flat = settings.startup['pollution-love-omnicrompression-pollution-add'].value
		local scale = settings.startup['pollution-love-omnicrompression-pollution-scale'].value

		for _, ptype in ipairs(types) do
			for name, building in pairs(data.raw[ptype]) do
				local _begin, _end, originalName, compressionLevel = name:find('(.-)%-compressed%-(.-)$')

				if not _begin then goto CONTINUE end

				local unmap = mapped[compressionLevel]

				if not unmap then
					log('Matched name against omnicompression: ' .. name .. ' in prototypes ' .. ptype .. ', but can\'t figure out numeric level of ' .. compressionLevel .. '!')
					goto CONTINUE
				end

				if
					type(building.energy_source) == 'table' and
					type(building.energy_source.emissions_per_minute) == 'number' and
					building.energy_source.emissions_per_minute ~= 0
				then
					local original = data.raw[ptype][originalName]

					if not original then
						log('Can\'t find original building ' .. originalName .. ' in ' .. ptype .. ' for ' .. name .. '!')
						goto CONTINUE
					end

					if type(original.energy_source) == 'table' and type(original.energy_source.emissions_per_minute) == 'number' then
						if
							setting == 'BOTH' or
							(setting == 'PRODUCERS' or setting == 'BOTH_BUT_CONSUMERS_LINEAR') and building.energy_source.emissions_per_minute > 0 or
							setting == 'CONSUMERS' and building.energy_source.emissions_per_minute < 0
						then
							building.energy_source.emissions_per_minute = original.energy_source.emissions_per_minute * math.pow(multiplier, (unmap * scale + flat) * power)
						elseif setting == 'BOTH_BUT_CONSUMERS_LINEAR' and building.energy_source.emissions_per_minute < 0 then
							building.energy_source.emissions_per_minute = original.energy_source.emissions_per_minute * math.pow(multiplier, unmap)
						end
					end
				end

				::CONTINUE::
			end
		end
	end
end
